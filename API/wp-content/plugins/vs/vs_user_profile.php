<?php

class VS_User_Profile {
	
	function __construct() {
		add_action( 'show_user_profile', __CLASS__ . '::user_profil_customer_id_field' );
		add_action( 'edit_user_profile', __CLASS__ . '::user_profil_customer_id_field' );
		
		add_action( 'personal_options_update',array( $this, 'user_profil_customer_id_field1' ) );
		add_action( 'edit_user_profile_update',array( $this, 'user_profil_customer_id_field1' ) );
	}
	
	function user_profil_customer_id_field($user){
		$html .= '<table class="form-table">
					<tr>
						<th><label for="fullname">Name</label></th>
						<td>
							<input type="text" name="fullname" id="fullname" class="regular-text" value="'.get_usermeta($user->ID, 'fullname', true).'" />
						</td>
					</tr> 
					<tr>
						<th><label for="street">Strasse</label></th>
						<td>
							<input type="text" name="street" id="street" class="regular-text" value="'.get_usermeta($user->ID, 'street', true).'" />
						</td>
					</tr> 
					<tr>
						<th><label for="zipcode">Postleitzahl</label></th>
						<td>
							<input type="text" name="zipcode" id="zipcode" class="regular-text" value="'.get_usermeta($user->ID, 'zipcode', true).'" />
						</td>
					</tr> 
					<tr>
						<th><label for="city">Stadt</label></th>
						<td>
							<input type="text" name="city" id="city" class="regular-text" value="'.get_usermeta($user->ID, 'city', true).'" />
						</td>
					</tr> 
					<tr>
						<th><label for="phone">Telefon</label></th>
						<td>
							<input type="text" name="phone" id="phone" class="regular-text" value="'.get_usermeta($user->ID, 'phone', true).'" />
						</td>
					</tr> 
					<tr>
						<th><label for="lat">Lat</label></th>
						<td>
							<input type="text" name="lat" id="lat" class="regular-text" value="'.get_usermeta($user->ID, 'lat', true).'" />
						</td>
					</tr> 
					<tr>
						<th><label for="lon">Lon</label></th>
						<td>
							<input type="text" name="lon" id="lon" class="regular-text" value="'.get_usermeta($user->ID, 'lon', true).'" />
						</td>
					</tr>
				</table>  ';
		echo $html;
	}
	
	function user_profil_customer_id_field1($user){
		
		$geo = $this->get_open_street_map_data($_REQUEST);
		
		update_usermeta($user, 'fullname', $_REQUEST['fullname'] );
		update_usermeta($user, 'street', $_REQUEST['street'] );
		update_usermeta($user, 'zipcode', $_REQUEST['zipcode'] );
		update_usermeta($user, 'city', $_REQUEST['city'] );
		update_usermeta($user, 'phone', $_REQUEST['phone'] );
		update_usermeta($user, 'lat', $geo['lat'] );
		update_usermeta($user, 'lon', $geo['lon'] );
	}
	
	function get_open_street_map_data($data){	
		$open_street_map_url = 'http://nominatim.openstreetmap.org/search?q='.$data['zipcode'].'+'.$data['city'].',+'.$_REQUEST['street'].'&limit=5&format=json&addressdetails=1';
		$opts = array('http'=>array('header'=>"User-Agent: StevesCleverAddressScript 3.7.6\r\n"));
		$context = stream_context_create($opts);
		$openstreetmap = file_get_contents($open_street_map_url, false, $context);
		$openstreetmap_json = json_decode($openstreetmap);
		
		return array(
			'lat' => $openstreetmap_json[0]->lat,
			'lon' => $openstreetmap_json[0]->lon,
		);
	} 		
}

new VS_User_Profile();
