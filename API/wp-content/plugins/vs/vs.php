<?php
/**
 * Plugin Name: Vereinssoftware Portal
 * Description: Vereinssoftware Portal | API | NUTZER | POSTS | MESSAGES
 * Version: 1.0
 * Author: Robert Erdmann
 */

require_once('vs_post_type.php');
require_once('vs_user_profile.php');

class Vereinssoftware {
	
	function __construct() {
		
		
         if (is_admin()) {
			 
		 }
		 
		 add_action( 'wp_enqueue_scripts',array( $this, 'vs_enqueue_scripts_frontend' ) );
	}
	
	function vs_enqueue_scripts_frontend($hook) {
            wp_enqueue_style( 'vs_frontend_css', plugins_url('css/frontend-style.css', __FILE__) );
            wp_enqueue_script( 'vs_frontend_script', plugins_url('js/frontend-scripts.js', __FILE__), array('jquery') );  
    }

}

new Vereinssoftware();