<?php

/*
 * ich biete / ich suche
 * //Title
 * Galerie
 * Kategorie
 * 	Katzen -> Hauskatze
 * 	Katzen -> Rasse?
 * 	Katzen -> Zubehör
 *	Hund -> Rasse?
 *	Hund -> Zubemobile 
 * // Preis
 * // VHB / Festpreis / verschenken
 * //Beschreibung
 * Standort / PLZ / ORT
 * Rasse
 * Alter
 * Geimpft / gechipt
 * Muttertier kann besichtigt werden
 * Angebotstyp / Gesuch / Angebot
 * LAT AND LONG
 * DATE CREATE / UPDATED DATE
 *
 *
 *
 *
 *
 *
 */
class VS_Post_Type {
	
	public $offer_type = array(
		'offer' => 'Angebot',
		'request' => 'Gesuch'
	); 
	
	public $price_type = array(
		'basisfornegotiation' => 'Verhandlungsbasis',
		'fixedprice' => 'Fixpreis',
		'togiveaway' => 'Zu verschenken'
	);
	
	function __construct() {
		add_action( 'init', array($this,'post_type_advertisement') );
		add_filter ('ocean_gallery_metabox_post_types', array($this, 'prefix_gallery_metabox'));
		
		
		add_action( 'add_meta_boxes', array( $this,'advertisement_meta_box_price') );
		add_action( 'post_updated',  array( $this,'advertisement_save_meta_data'), 10, 3 ); 

	}

	function post_type_advertisement() {
		$labels = array(
			'name'                => _x( 'Kleinanzeigen', 'Post Type General Name', 'vs' ),
			'singular_name'       => _x( 'Kleinanzeige', 'Post Type Singular Name', 'vs' ),
			'menu_name'           => __( 'Kleinanzeigen', 'vs' ),
			'parent_item_colon'   => __( 'Übergeordnete Kleinanzeige', 'vs' ),
			'all_items'           => __( 'Alle Kleinanzeige', 'vs' ),
			'view_item'           => __( 'Kleinanzeige anzeigen', 'vs' ),
			'add_new_item'        => __( 'neue Kleinanzeige', 'vs' ),
			'add_new'             => __( 'Hinzufügen', 'vs' ),
			'edit_item'           => __( 'Kleinanzeige bearbeiten', 'vs' ),
			'update_item'         => __( 'Update Kleinanzeige', 'vs' ),
			'search_items'        => __( 'Suche nach Kleinanzeige', 'vs' ),
			'not_found'           => __( 'Nicht gefunden', 'vs' ),
			'not_found_in_trash'  => __( 'Nicht gefunden im Papierkorb', 'vs' ),
		);   

		$args = array(
			'label'               => __( 'advertisement', 'vs' ),
			'description'         => __( 'Kleinanzeigen', 'vs' ),
			'labels'              => $labels,
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
			'taxonomies'          => array('advertisement', 'category' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 2,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'page',
			'taxonomies'          => array( 'category' ),
		);
		
		register_post_type( 'advertisement', $args );
		register_taxonomy("categories", array("test"), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => 'test', 'with_front'=> false )));
	
	}
	
	function prefix_gallery_metabox( $types ) {
		$types[] = 'advertisement';
		return $types;
	}
	
	function advertisement_meta_box_price(){
		
		add_meta_box(
			'advertisement_description',
			__( 'Beschreibung' ),
			array($this,'add_advertisement_description'),
			'advertisement',
			'normal',
			'default'
		);
		
		add_meta_box(
			'advertisement_price',
			__( 'Preis' ),
			array($this,'add_advertisement_price_type'),
			'advertisement',
			'normal',
			'default'
		);
		
		add_meta_box(
			'advertisement_offer_type',
			__( 'Anzeigentyp' ),
			array($this,'add_advertisement_offer_type'),
			'advertisement',
			'normal',
			'default'
		);



	}
	
	function add_advertisement_description( $post ){
		$meta = get_post_meta($post->ID, 'advertisement_description', true);
		echo '<div class="advertisement_description">
					<textarea type="text" class="advertisement_description" id="advertisement_description" name="advertisement_description" palceholder="kurzbeschreibung"/>'.$meta.'</textarea>
				</div>
			';
	}
	
	function add_advertisement_price( $post ){
		$meta = get_post_meta($post->ID, 'advertisement_price', true);
		echo '<div class="advertisement_price">
					<input type="number" step=".01" style="width:100%" class="advertisement_price" id="advertisement_price" name="advertisement_price" value="'.$meta.'" palceholder="price"/>
				</div>
			';
	}
	
	
	
	
	function add_advertisement_price_type($post){
		echo $this->get_price_type($post);
	}
	
	function get_price_type($post){
		$meta = get_post_meta($post->ID, 'advertisement_price_type', true);
		$html = '<select name="advertisement_price_type">';
		foreach($this->price_type as $key => $val ){
			$selected = '';
			if($key == $meta){
				$selected = 'selected';
			}
			$html .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
		}
		$html .= '</select>';
		
		return $html;
	}
	
	
	
	
	
	
	function add_advertisement_offer_type($post){
		echo $this->get_offer_type($post);
	}
	
	function get_offer_type($post){
		$meta = get_post_meta($post->ID, 'advertisement_offer_type', true);
		$html = '<select name="advertisement_offer_type">';
		foreach($this->offer_type as $key => $val ){
			$selected = '';
			if($key == $meta){
				$selected = 'selected';
			}
			$html .= '<option value="'.$key.'" '.$selected.'>'.$val.'</option>';
		}
		$html .= '</select>';
		
		return $html;
	}
	
	function advertisement_save_meta_data($post_id){
		update_post_meta($post_id, 'advertisement_price',$_REQUEST['advertisement_price']);
		update_post_meta($post_id, 'advertisement_price_type',$_REQUEST['advertisement_price_type']);
		update_post_meta($post_id, 'advertisement_offer_type',$_REQUEST['advertisement_offer_type']);
		update_post_meta($post_id, 'advertisement_description',$_REQUEST['advertisement_description']);

		
	}

	
	
	
}

new VS_Post_Type();
